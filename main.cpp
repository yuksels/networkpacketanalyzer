#include <iostream>
#include <pcap.h>
#include <regex.h>
#include <thread>

int main()
{
	char *dev, errbuf[PCAP_ERRBUF_SIZE];
	dev = pcap_lookupdev(errbuf);
	if (dev == NULL) {
		fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
		return(2);
	}
	std::cout<< "Device: "<< dev <<std::endl;
}
